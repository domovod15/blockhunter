/**
* Create Zhigulin Dmitriy
*
* This class contains all the basic functions and variables to control the game
*/

'use strict';

class BlockHunterEngine {

    constructor() {
        //size area     
        this.bodyParams = {
            width:320,
            height:320
        }
    
        
        //param avatar
        this.itemParams = {
            step:40, //offest by x/y if press button controllers
            width:40, //width item
            height:60, //height item
        }
        
        //paramsfor block
        this.wallParams = {
            maxItems:6, //max walls in area
            minMarginPrevElement:160, // minimum distance from the previous element
            maxMarginLeft:240,// the maximum offset from the field area. due to the size of the wall //TODO create an automatic calculation of wall dimensions
            widthItem:80,
            heightItem:40,
            sizeStep:4,// the size of the shift at the interation (px)
        }
        
        this.timerDefaultParams = {
            timeInterval:100,//ms for one offset walls частота движения стен
            level:1,//the reduction ratio of the interval. With each passing level, we will accelerate the movement of the walls.
            stepLevel:15,//interval reduction time.That is, each level of the wall offset frequency will be reduced by as many milliseconds
            timeNextLevel:5,//at what time of the game the next level will be started, i.e. how many seconds of the game should pass after start play
            timeNextLevelStep:5,//how many seconds do need to play at this level to go to the next
            active:0, // 0 - stop game ; 1 - play game
            playTime:0,//time play
            minTimeInterval:5, // in fact, this is the minimum step time of the walls. I. e. 5 milliseconds is the maximum refresh rate, the maximum level.
            endGame:0,// flag to end game
        }
        
        this.timerParams = {
            timeInterval:100,//ms for one offset walls частота движения стен
            level:1,//the reduction ratio of the interval. With each passing level, we will accelerate the movement of the walls.
            stepLevel:15,//interval reduction time.That is, each level of the wall offset frequency will be reduced by as many milliseconds
            timeNextLevel:5,//at what time of the game the next level will be started, i.e. how many seconds of the game should pass after start play
            timeNextLevelStep:5,//how many seconds do need to play at this level to go to the next
            active:0, // 0 - stop game ; 1 - play game
            playTime:0,//time play
            minTimeInterval:5,// in fact, this is the minimum step time of the walls. I. e. 5 milliseconds is the maximum refresh rate, the maximum level.
            endGame:0,//flag to end game
        }
        
        this.scoreData = {
            timePlay:0,//how many seconds in the current game
            score:0,// total score
            scoreStep:1,// points for step
        }
        
        this.scoreDataDefault = {
            timePlay:0,//how many seconds in the current game
            score:0,// total score
            scoreStep:1,// points for step
        }
        
        
        
    };
      
        
    /**
    * This feature listens to button presses and controls the hero
    *
    * @param e event in eventList
    */
    moveRect(e){
        
        e.preventDefault();
        
        var step=this.itemParams.step;

        if(this.timerParams.endGame==1)
        {
            return;
        }

        var item = document.getElementById("item");
        // get style for item
        var cs = window.getComputedStyle(item);
        
        var leftSize = parseInt(cs.left);
        var topSize = parseInt(cs.top);
        
        var jqItem=$("#item");
        var parent=jqItem.parent();
        
        var wItem=jqItem.width();
        var hItem=jqItem.height();
        
        var wParent=parent.width()-wItem;
        var hParent=parent.height()-hItem;
        
        
        switch(e.keyCode){
            case 32:  // if space
                e.preventDefault();
                if(this.timerParams.active===0)
                {
                    this.timerParams.active=1;
                    this.actions();
                }
                else
                {
                    this.timerParams.active=0
                }
                break;
            case 37:  // left
                e.preventDefault();
                if(leftSize>0)
                {
                    if(this.timerParams.active===0)
                    {
                        return;
                    }
                    
                    let newLeft=leftSize-step;
                    if(newLeft<0)
                    {
                        newLeft=0;
                    }
                    if(newLeft>wParent)
                    {
                        newLeft=wParent;
                    }	
                    item.style.left = newLeft+ "px";
                }					
                break;
            case 38:   // up
                e.preventDefault();
                if(topSize>0)
                {
                    if(this.timerParams.active===0)
                    {
                        return;
                    }
                    
                    let newTop=topSize-step;
                    if(newTop<0)
                    {
                        newTop=0;
                    }
                    if(newTop>hParent)
                    {
                        newTop=hParent;
                    }				
                    item.style.top = newTop + "px";
                }
                    
                break;
            case 39:   // right
                e.preventDefault();
                if(leftSize<wParent)
                {
                    if(this.timerParams.active===0)
                    {
                        return;
                    }
                    
                    let newLeft=leftSize+step;				
                    if(newLeft>wParent)
                    {
                        newLeft=wParent;
                    }	
                    item.style.left = newLeft+ "px";
                }	
                
                break;
            case 40:   // bottom
                e.preventDefault();
                if(topSize<hParent)
                {
                    if(this.timerParams.active===0)
                    {
                        return;
                    }
                    
                    let newTop=topSize+step;
                    if(newTop<0)
                    {
                        newTop=0;
                    }
                    if(newTop>hParent)
                    {
                        newTop=hParent;
                    }				
                    item.style.top = newTop + "px";
                }
                break;
        }
    }

    /**
    * calculates the sum of points
    */
    sumScore()
    {		

        var level=parseFloat(this.timerParams.level.toFixed(0));
        
        var score=parseFloat(this.scoreData.score.toFixed(0));
        var scoreStep=this.scoreData.scoreStep.toFixed(1);
        
        //---------------
        var newScore=score+scoreStep*level;		
        newScore++;newScore--;
        newScore.toFixed(0);
        
        this.scoreData.score=newScore;
        //-------		
       
        var newTime=parseFloat(this.timerParams.playTime);
        
        newTime.toFixed(1);
        this.scoreData.timePlay=newTime;
    }
    
    /**
    * displays the user the quantity of points
    */
    viewScore()
    {
        this.sumScore();
        
        $("#timePlay").html(this.scoreData.timePlay.toFixed(1));
        $("#score").html(this.scoreData.score);
    }
    
    /**
    * This function calculates and displays the better state of the points
    */
    viewScoreNice()
    {
        this.sumScore();
        var veryTimePlay=parseFloat($("#veryTimePlay").html());
        
        var veryScore=parseFloat($("#veryScore").html());
        
        var timePlay=parseFloat(this.scoreData.timePlay);
        var score=parseFloat(this.scoreData.score);
        
        if(timePlay>veryTimePlay)
        {
            $("#veryTimePlay").html(this.scoreData.timePlay);
        }
        
        if(score>veryScore)
        {
            $("#veryScore").html(this.scoreData.score);
        }
        
        //and alert for restart
        alert("End game. Press Restart");        
        
    }
    
    /**
    * Set start(default) parameters
    */
    setDefaultParam()
    {
        this.timerParams.timeInterval=this.timerDefaultParams.timeInterval;
        this.timerParams.level=this.timerDefaultParams.level;
        this.timerParams.stepLevel=this.timerDefaultParams.stepLevel;
        this.timerParams.timeNextLevel=this.timerDefaultParams.timeNextLevel;
        this.timerParams.timeNextLevelStep=this.timerDefaultParams.timeNextLevelStep;
        this.timerParams.active=this.timerDefaultParams.active;
        this.timerParams.playTime=this.timerDefaultParams.playTime;
        this.timerParams.minTimeInterval=this.timerDefaultParams.minTimeInterval;
        this.timerParams.endGame=this.timerDefaultParams.endGame;
        
        this.scoreData.timePlay=this.scoreDataDefault.timePlay;
        this.scoreData.score=this.scoreDataDefault.score;
        this.scoreData.scoreStep=this.scoreDataDefault.scoreStep;
        
    }
    
    
    
    /**
    * Returns a random number
    */
    getRandomArbitary(min, max)
    {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    
    /**
    * MAGIC
    * Returns the index of the last wall block. That is, the sequence number of the last element.
    */
    getIndexlastWall()
    {
        return $("#areaPlay .wall").length-1;
    }
    
    /**
    * create html for walls
    */
    createBlockWall()
    {
        return '<div class="wall"></div>';
    }
    
    /**
    * creates a walls and places them correctly
    */
    createWall()
    {
        //first, let's count how many elements we have on the field.
        var wallsCount=$("#areaPlay .wall").length;
        
        //number of the last wall created
        var lastWall=$("#areaPlay .wall:eq("+(wallsCount-1)+")");
        
        if(wallsCount>0)
        {
            //now we need to understand at what height is the previous wall        
            var lastWallTop=lastWall.position().top;
        }
        else
        {
            var lastWallTop=0;
        }
        
        //if we have created enough walls, that don't make them anymore
        if(wallsCount>=this.wallParams.maxItems)
        {
            return false;
        }
       
        
        //create an offset for a new wall
        var newWallTop=lastWallTop-this.wallParams.minMarginPrevElement;
        if(-newWallTop==this.wallParams.minMarginPrevElement)
        {
            newWallTop=0;
        }
        //geting left offset
        var newWallLeft=this.getRandomArbitary(0, this.wallParams.maxMarginLeft);
        
        //make and insert one wall
        var block=this.createBlockWall();
        $("#areaPlay").append(block);
        //get the index of the last element
        var index=this.getIndexlastWall();
        
        //set offsets
        $("#areaPlay .wall:eq("+(index)+")").css({"top":newWallTop+"px","left":newWallLeft+"px"});
           
    }
    
    
    /**
    * The function bypasses all blocks of walls on the area and lowers them down by the size of a single step
    */
    setOneStepWalls()
    {
        var localObj=this;
        
        $("#areaPlay .wall").each(function(){
            
            var posTop=$(this).position().top;
            posTop=posTop+localObj.wallParams.sizeStep;
            $(this).css({"top":posTop+"px"});
            
        });
    }
    
    
    /**
    * if the wall fell below the playing field, we can remove it
    */    
    deleteWall()
    {
        var localObj=this;
        
        //TODO сделать так, чтобы проверялась только последняя стена, а не все (check only last wall but not all)
        
        $("#areaPlay .wall").each(function(){
            
            var posTop=$(this).position().top;
            if(posTop>(localObj.wallParams.heightItem+localObj.bodyParams.height))
            {
                $(this).remove();
            }
            
        });
        
    }
    
    
    /**
    * executes commands to check states and positions. Controling the game.
    */
    actions()
    {
        if(this.timerParams.endGame===1)
        {
            return;
        }
        //make wals
        this.createWall();
        //set offsets walls
        this.setOneStepWalls();
        //delete wals
        this.deleteWall();
        //check collisions
        this.checkCoverBlock();
        // and run again        
        if(this.timerParams.active===1)
        {
            //calculate the time interval for the next step
            var timeInterval=this.timerParams.timeInterval-(this.timerParams.level*this.timerParams.stepLevel);
            //check whether the maximum level is reached, if Yes, then leave the minimum time for the interval
            if(timeInterval<this.timerParams.minTimeInterval)
            {
                timeInterval=this.timerParams.minTimeInterval;                
            }			
            //make a note that at this level we have made a step
            this.timerParams.playTime+=timeInterval/1000;//here ms
            //check next level
            if(this.timerParams.playTime>=this.timerParams.timeNextLevel)
            {
                var checkLvl=true;
            }
            
            if(checkLvl)
            {
                //we have moved to a new level
                this.timerParams.level=this.timerParams.level+1;
                this.timerParams.timeNextLevel=this.timerParams.timeNextLevel+this.timerParams.timeNextLevelStep;
            }
            
            //set the interval for the next execution             
            let localObj=this;  
            setTimeout(function() {
                    localObj.actions(); 
                }, timeInterval);
        }
        
    }
    
    
    /**
    * checks the position of all items and avatar in the play area.
    */
    checkCoverBlock()
    {
        var localObj=this;
        var item=$("#item");
        //getting position avatar
        var leftMin=item.position().left;
        var topMin=item.position().top;
        var leftMax=leftMin+this.itemParams.width;
        var topMax=leftMin+this.itemParams.height;
        
        var Item={
            "x":leftMin,
            "y":topMin,
            "width":this.itemParams.width,
            "height":this.itemParams.height,
        };
         
        //check the walls and see if there are any intersections
        $("#areaPlay .wall").each(function(){
            
            var posWallTop=$(this).position().top;
            var posWallLeft=$(this).position().left;
            var posWallTopMax=posWallTop+localObj.wallParams.heightItem;
            var posWallLeftMax=posWallLeft+localObj.wallParams.widthItem;
            
            var Wall={
                "x":posWallLeft,
                "y":posWallTop,
                "width":localObj.wallParams.widthItem,
                "height":localObj.wallParams.heightItem,
            };
            
            
            if(localObj.MacroCollision(Item,Wall))
            {
                localObj.timerParams.active=0;
                localObj.timerParams.endGame=1;
                localObj.viewScore();
                localObj.viewScoreNice();
            }
            else
            {
                localObj.viewScore();
            }
            
        
            
        });
        
    }
    /**
    * check collisions
    */
    MacroCollision(Item,Wall)
    {
        var XColl=false;
        var YColl=false;
        
        if (
            (Item.x + Item.width >= Wall.x)
            &&
            (Item.x <= Wall.x + Wall.width)
        ){
           XColl = true; 
        } 
        if (
            (Item.y + Item.height >= Wall.y)
            &&
            (Item.y <= Wall.y + Wall.height)
        ) { 
            YColl = true;
        }
        
        
        if ( XColl && YColl ) {
            return true;
        }
        
        return false;
    }
}