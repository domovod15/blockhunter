var GameClass = new BlockHunterEngine();

$(document).ready(function(){
    
    
    addEventListener("keydown", function(e) {
         switch(e.keyCode){
            case 32:  // if space
                e.preventDefault();                
                break;
            case 37:  // left
                e.preventDefault();
                break;
            case 38:   // up
                e.preventDefault();
                break;
            case 39:   // right
                e.preventDefault();                
                break;
            case 40:   // bottom
                e.preventDefault();                
                break;
        }
    });
    addEventListener("keyup", function(e){
        GameClass.moveRect(e);
    });
    
	$("#restart").on("click",function(){
        
		$("#areaPlay .wall").remove();		
		GameClass.setDefaultParam();
		GameClass.viewScore(); 
	});
});

